const bp = require('body-parser');
const { MongoClient, ObjectId } = require("mongodb");
const express = require('express');

const uri = "mongodb+srv://app-user:app-password@todo-list.pfzua.mongodb.net/todolist?retryWrites=true&w=majority";
const nameDB = "todo-list";
const nameColl = "todolist";

const PORT = 3001;
const app = express();

let database, collection;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.use('/addItem', bp.json());
app.use('/deleteItem', bp.json());

app.get("/", async(req, res) => {
    let dblist = [];
    await collection.find({ }).forEach(r => dblist.push(r));
    res.json(dblist);
})

app.post("/addItem", async(req, res) => {
    await collection.insertOne(req.body, (err, result) => {
        if(err) {
            return res.status(500).send(err)
        }
        res.send(result.ops[0]);
    });
})

app.delete("/deleteItem", async(req, res) => {
    await collection.deleteOne({ _id: ObjectId(req.body._id)}, (err, result) => {
        if(err) {
            return res.status(500).send(err);
        }
        res.send(`Item ${req.body._id} deleted successfully!`);
    })
})

app.listen(PORT, () => {
    console.log(`Server listening on ${PORT}`);
     MongoClient.connect(uri, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      }, (error, client) => {
        if(error) {
            throw error;
        }
        database = client.db(nameDB);
        collection = database.collection(nameColl);
        console.log(`Connected to ${nameDB} and ${nameColl}` );
    });
})
